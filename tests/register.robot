*** Settings ***
Resource    ../resources/resource.robot
Resource    ../resources/pages/register.robot


*** Test Cases ***
Cenário 01: Cadastro de usuário com dados válidos
    Dado que eu tenha acessado a API de cadastro de usuário
    Quando eu enviar uma solicitação POST com as informações válidas
    Então a resposta deve ter o status 201
    E a resposta deve conter a mensagem no campo "message" "Cadastro realizado com sucesso"

Cenário 02: Cadastro de usuário com dados inválidos
    Dado que eu tenha acessado a API de cadastro de usuário
    Quando eu enviar uma solicitação POST com as informações inválidas
    Então a resposta deve ter o status 400
    E a resposta deve conter a mensagem no campo "email" "email deve ser um email válido"

Cenário 03: Cadastro de usuário com email já cadastrado
    Dado que eu tenha acessado a API de cadastro de usuário
    Quando eu enviar uma solicitação POST com as informações já cadastradas
    Então a resposta deve ter o status 400
    E a resposta deve conter a mensagem no campo "message" "Este email já está sendo usado"

Cenário 04: Consulta de usuário recém-cadastrado
    Dado que eu tenha acessado a API de cadastro de usuário
    Quando eu enviar uma solicitação GET para "/usuarios/id"
    Então a resposta deve ter o status 200
    E a resposta deve conter as informações do usuário cadastrado

Cenário 05: Edição de usuário
    Dado que eu tenha acessado a API de cadastro de usuário
    Quando eu enviar uma solicitação PUT para "/usuarios/id"
    Então a resposta deve ter o status 200
    E a resposta deve conter a mensagem no campo "message" "Registro alterado com sucesso"
    E as informações do usuário devem ser atualizadas com os novos valores

Cenário 06: Edição de usuário com email já cadastrado
    Dado que eu tenha acessado a API de cadastro de usuário
    Quando eu enviar uma solicitação PUT para "/usuarios/id" com email já cadastrado
    Então a resposta deve ter o status 400
    E a resposta deve conter a mensagem no campo "message" "Este email já está sendo usado"

Cenário 07: Deleção de usuário
    Dado que eu tenha acessado a API de cadastro de usuário
    Quando eu enviar uma solicitação DELETE para "/usuarios/id"
    Então a resposta deve ter o status 200
    E a resposta deve conter a mensagem no campo "message" "Registro excluído com sucesso"

Cenário 08: Deleção de usuário inexistente
    Dado que eu tenha acessado a API de cadastro de usuário
    Quando eu enviar uma solicitação DELETE para "/usuarios/id" com um ID inválido
    Então a resposta deve ter o status 200
    E a resposta deve conter a mensagem no campo "message" "Nenhum registro excluído"

Cenário 09: Listagem de usuários
    Dado que eu tenha acessado a API de cadastro de usuário
    Quando eu enviar uma solicitação GET para "/usuarios"
    Então a resposta deve ter o status 200
    E a resposta deve conter as informações dos usuários cadastrados

Cenário 10: Busca de usuário inexistente por ID
    Dado que eu tenha acessado a API de cadastro de usuário
    Quando eu enviar uma solicitação GET para "/usuarios/id" com um ID inválido
    Então a resposta deve ter o status 400
    E a resposta deve conter a mensagem no campo "message" "Usuário não encontrado"