*** Settings ***
Resource    ../resources/resource.robot
Resource    ../resources/pages/login.robot


*** Test Cases ***
Cenário 01: Login bem sucedido
    Dado que eu tenha acessado a API de login
    Quando o usuário tem um email válido e uma senha válida
    Então a API deve retornar um token de acesso válido

Cenário 02: Tentativa de login sem informar email e/ou senha
    Dado que eu tenha acessado a API de login
    Quando o usuário não informou o email e/ou senha
    # Então a API deve retornar uma mensagem de erro informando que o email e senha não podem ficar em branco

Cenário 03: Tentativa de login com email inválido
    Dado que eu tenha acessado a API de login
    Quando o usuário informou um email inválido e uma senha válida
    E a resposta deve conter a mensagem no campo "email" "email deve ser um email válido"

Cenário 04: Tentativa de login com senha inválida
    Dado que eu tenha acessado a API de login
    Quando o usuário informou um email válido e uma senha inválida
    E a resposta deve conter a mensagem no campo "message" "Email e/ou senha inválidos"

Cenário 05: Tentativa de login com email e senha inválidos
    Dado que eu tenha acessado a API de login
    Quando o usuário informou um email inválido e uma senha inválida
    E a resposta deve conter a mensagem no campo "email" "email deve ser um email válido"
