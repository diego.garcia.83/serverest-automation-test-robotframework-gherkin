*** Settings ***
Library     RequestsLibrary
Library     JSONLibrary
Library     String


*** Keywords ***
Chamada Rest DELETE
    [Arguments]    ${URL}
    ${response}    DELETE On Session
    ...    alias=ServeRest
    ...    url=${URL}
    ...    expected_status=anything
    Set Test Variable    ${RESPONSE}    ${response.json()}

Chamada Rest GET
    [Arguments]    ${URL}
    ${response}    GET On Session
    ...    alias=ServeRest
    ...    url=${URL}
    ...    expected_status=anything
    Set Test Variable    ${RESPONSE}    ${response.json()}

Chamada Rest POST Login
    [Arguments]    ${EMAIL}    ${PASSWORD}    ${URL}
    ${BODY}    Create Dictionary
    ...    email=${EMAIL}
    ...    password=${PASSWORD}
    ${response}    POST On Session
    ...    alias=ServeRest
    ...    url=${URL}
    ...    json=${BODY}
    ...    expected_status=anything
    IF    ${response.status_code} == 201
        Set Test Variable    ${ID_USER}    ${response.json()["_id"]}
    END
    Set Test Variable    ${RESPONSE}    ${response.json()}

Chamada Rest POST Register
    [Arguments]    ${NOME}    ${EMAIL}    ${PASSWORD}    ${URL}
    ${BODY}    Create Dictionary
    ...    nome=${NOME}
    ...    email=${EMAIL}
    ...    password=${PASSWORD}
    ...    administrador=true
    ${response}    POST On Session
    ...    alias=ServeRest
    ...    url=${URL}
    ...    json=${BODY}
    ...    expected_status=anything
    IF    ${response.status_code} == 201
        Set Test Variable    ${ID_USER}    ${response.json()["_id"]}
    END
    Set Test Variable    ${RESPONSE}    ${response.json()}

Chamada Rest PUT
    [Arguments]    ${NOME}    ${EMAIL}    ${PASSWORD}    ${ADMIN}    ${URL}
    ${BODY}    Create Dictionary
    ...    nome=${NOME}
    ...    email=${EMAIL}
    ...    password=${PASSWORD}
    ...    administrador=${ADMIN}
    ${response}    PUT On Session
    ...    alias=ServeRest
    ...    url=${URL}
    ...    json=${BODY}
    ...    expected_status=anything
    Set Test Variable    ${RESPONSE}    ${response.json()}