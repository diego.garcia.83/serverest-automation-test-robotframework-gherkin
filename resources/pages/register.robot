*** Settings ***
Library     String
Library     RequestsLibrary
Library     Collections
Resource    ../resource.robot


*** Keywords ***
Dado que eu tenha acessado a API de cadastro de usuário
    ${headers}    Create Dictionary
    ...    accept=application/json
    ...    Content-Type=application/json
    Create Session    alias=ServeRest    url=https://serverest.dev    headers=${headers}

Quando eu enviar uma solicitação POST com as informações válidas
    ${randonNumber}    Generate Random String    length=4    chars=[NUMBERS]
    Set Test Variable    ${NOME}    automation.${randonNumber}
    Set Test Variable    ${EMAIL}    ${NOME}@automation.com
    Set Test Variable    ${PASSWORD}    1234
    Set Test Variable    ${URL}    /usuarios
    Chamada Rest POST Register    ${NOME}    ${EMAIL}    ${PASSWORD}    ${URL}

Quando eu enviar uma solicitação POST com as informações inválidas
    ${randonNumber}    Generate Random String    length=4    chars=[NUMBERS]
    Set Test Variable    ${NOME}    automation.${randonNumber}
    Set Test Variable    ${EMAIL}    ${NOME}automation.com
    Set Test Variable    ${PASSWORD}    1234
    Set Test Variable    ${URL}    /usuarios
    Chamada Rest POST Register    ${NOME}    ${EMAIL}    ${PASSWORD}    ${URL}

Quando eu enviar uma solicitação POST com as informações já cadastradas
    Quando eu enviar uma solicitação POST com as informações válidas
    Chamada Rest POST Register    ${NOME}    ${EMAIL}    ${PASSWORD}    ${URL}

Quando eu enviar uma solicitação GET para "/usuarios/id"
    Quando eu enviar uma solicitação POST com as informações válidas
    Set Test Variable    ${URL}    /usuarios/${ID_USER}
    Chamada Rest GET    ${URL}

Quando eu enviar uma solicitação PUT para "/usuarios/id"
    Quando eu enviar uma solicitação POST com as informações válidas
    ${randonNumber}    Generate Random String    length=8    chars=[NUMBERS]
    Set Test Variable    ${NOME}    automation.${randonNumber}
    Set Test Variable    ${EMAIL}    ${NOME}@automation.com
    Set Test Variable    ${PASSWORD}    12345678
    Set Test Variable    ${ADMIN}    false
    Set Test Variable    ${URL}    /usuarios/${ID_USER}
    Chamada Rest PUT    ${NOME}    ${EMAIL}    ${PASSWORD}    ${ADMIN}    ${URL}

Quando eu enviar uma solicitação PUT para "/usuarios/id" com email já cadastrado
    Quando eu enviar uma solicitação POST com as informações válidas
    Set Test Variable    ${EMAILPOST}    ${EMAIL}
    Quando eu enviar uma solicitação POST com as informações válidas
    ${randonNumber}    Generate Random String    length=8    chars=[NUMBERS]
    Set Test Variable    ${NOME}    automation.${randonNumber}
    Set Test Variable    ${EMAIL}    ${EMAILPOST}
    Set Test Variable    ${PASSWORD}    12345678
    Set Test Variable    ${ADMIN}    false
    Set Test Variable    ${URL}    /usuarios/${ID_USER}
    Chamada Rest PUT    ${NOME}    ${EMAIL}    ${PASSWORD}    ${ADMIN}    ${URL}

Quando eu enviar uma solicitação DELETE para "/usuarios/id"
    Quando eu enviar uma solicitação POST com as informações válidas
    Set Test Variable    ${ID_USER}    ${RESPONSE["_id"]}
    Set Test Variable    ${URL}    /usuarios/${ID_USER}
    Chamada Rest DELETE    ${URL}

Quando eu enviar uma solicitação DELETE para "/usuarios/id" com um ID inválido
    Quando eu enviar uma solicitação DELETE para "/usuarios/id"
    Set Test Variable    ${URL}    /usuarios/${ID_USER}
    Chamada Rest DELETE    ${URL}

Quando eu enviar uma solicitação GET para "/usuarios"
    Set Test Variable    ${URL}    /usuarios
    Chamada Rest GET    ${URL}

Quando eu enviar uma solicitação GET para "/usuarios/id" com um ID inválido
    Quando eu enviar uma solicitação DELETE para "/usuarios/id"
    Set Test Variable    ${URL}    /usuarios/${ID_USER}
    Chamada Rest GET    ${URL}

Então a resposta deve ter o status ${STATUS}
    Status Should Be    ${STATUS}

E a resposta deve conter a mensagem no campo "${CAMPO}" "${MSG}"
    Dictionary Should Contain Item    ${RESPONSE}    ${CAMPO}    ${MSG}

E a resposta deve conter as informações do usuário cadastrado
    Dictionary Should Contain Item    ${RESPONSE}    nome    ${NOME}
    Dictionary Should Contain Item    ${RESPONSE}    email    ${EMAIL}
    Dictionary Should Contain Item    ${RESPONSE}    password    1234
    Dictionary Should Contain Item    ${RESPONSE}    administrador    true
    Dictionary Should Contain Item    ${RESPONSE}    _id    ${ID_USER}

E as informações do usuário devem ser atualizadas com os novos valores
    Chamada Rest GET    ${URL}
    Dictionary Should Contain Item    ${RESPONSE}    nome    ${NOME}
    Dictionary Should Contain Item    ${RESPONSE}    email    ${EMAIL}
    Dictionary Should Contain Item    ${RESPONSE}    password    12345678
    Dictionary Should Contain Item    ${RESPONSE}    administrador    ${ADMIN}
    Dictionary Should Contain Item    ${RESPONSE}    _id    ${ID_USER}

E a resposta deve conter as informações dos usuários cadastrados
    Dictionary Should Contain Key    ${RESPONSE}    quantidade
    Dictionary Should Contain Key    ${RESPONSE}    usuarios