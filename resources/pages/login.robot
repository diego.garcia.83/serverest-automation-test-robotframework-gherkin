*** Settings ***
Library     String
Library     RequestsLibrary
Library     Collections
Resource    ../resource.robot
Resource    ../pages/register.robot


*** Keywords ***
Dado que eu tenha acessado a API de login
    ${headers}    Create Dictionary
    ...    accept=application/json
    ...    Content-Type=application/json
    Create Session    alias=ServeRest    url=https://serverest.dev    headers=${headers}

Quando o usuário tem um email válido e uma senha válida
    Quando eu enviar uma solicitação POST com as informações válidas
    Set Test Variable    ${URL}    /login
    Chamada Rest POST Login    ${EMAIL}    ${PASSWORD}    ${URL}

Quando o usuário não informou o email e/ou senha
    Set Test Variable    ${EMAIL}    ${EMPTY}
    Set Test Variable    ${PASSWORD}    ${EMPTY}
    Set Test Variable    ${URL}    /login
    Chamada Rest POST Login    ${EMAIL}    ${PASSWORD}    ${URL}

Quando o usuário informou um email inválido e uma senha válida
    Quando eu enviar uma solicitação POST com as informações válidas
    Set Test Variable    ${EMAIL}    @@@.com
    Set Test Variable    ${PASSWORD}    ${PASSWORD}
    Set Test Variable    ${URL}    /login
    Chamada Rest POST Login    ${EMAIL}    ${PASSWORD}    ${URL}

Quando o usuário informou um email válido e uma senha inválida
    Quando eu enviar uma solicitação POST com as informações válidas
    Set Test Variable    ${EMAIL}    ${EMAIL}
    Set Test Variable    ${PASSWORD}    4321
    Set Test Variable    ${URL}    /login
    Chamada Rest POST Login    ${EMAIL}    ${PASSWORD}    ${URL}

Quando o usuário informou um email inválido e uma senha inválida
    Quando eu enviar uma solicitação POST com as informações válidas
    Set Test Variable    ${EMAIL}    @@@.com
    Set Test Variable    ${PASSWORD}    4321
    Set Test Variable    ${URL}    /login
    Chamada Rest POST Login    ${EMAIL}    ${PASSWORD}    ${URL}

Então a API deve retornar um token de acesso válido
    Should Not Be Empty    ${RESPONSE["authorization"]}

Então a API deve retornar uma mensagem de erro informando que o email e senha não podem ficar em branco
    Dictionary Should Contain Item    ${RESPONSE}    email    email não pode ficar em branco
    Dictionary Should Contain Item    ${RESPONSE}    password    password não pode ficar em branco